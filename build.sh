#!/bin/bash

docker build -t "vkkotha/centos" --rm=true ./centos
docker build -t "vkkotha/centos:jdk7" --rm=true ./jdk7
docker build -t "vkkotha/centos:node" --rm=true ./node
docker build -t "vkkotha/centos:mongo" --rm=true ./mongo
